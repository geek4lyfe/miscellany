#!/usr/bin/env gawk -f
# https://www.linuxquestions.org/questions/linux-newbie-8/uniq-values-in-unsorted-file-4175471813/
# Unique lines from a file without sorting, keeping only the first occurance.
# If you want the lines prefixed by their count, then run:
#   script.awk -v c=1 file
#c = 0
BEGIN {
    #print "argc " ARGC
    #print "do_count " c
    #print "argv[0] " ARGV[0]
    #print "argv[1] " ARGV[1]
    #print "argv[2] " ARGV[2]
    #exit
}
{
    if (!counts[$0]++) {
        keys[k++] = $0
    }
}
END {
    for (i = 0; i < k; ++i) {
        key = keys[i]
        if ( c == 1 ) {
            print counts[key] " " key
        } else {
            print key
        }
    }
}
