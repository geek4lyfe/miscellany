#!/bin/sh
#exec /sbin/ifconfig | grep -E '\s+inet \w*\d+' | grep -Fv '127.0.0.1' | awk -F'[: ]+' '{print $4}'
exec /sbin/ifconfig | grep -E '\s+inet .*\d+' | grep -Fv '127.0.0.1' | perl -pe 's/\D+((?:\d{1,3}\.){3}\d{1,3}).+/$1/g'

