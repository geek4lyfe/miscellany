#!/bin/bash
# set -x

#cat ~/Downloads/AccountHistory.csv \
#	| csvfix money -r -f 5,6 | csvfix printf -fmt '%@%@%@%@%s+%@%@' \
#	| gtail -n +2 | tr -d "\n" | ghead -c -1 \
#	| cat <(echo -n "scale=2;") - <(echo "") | bc
cat ~/Downloads/AccountHistory.csv \
	| csvfix money -r -f 5,6 | csvfix printf -fmt '%@%@%@%@%s+%@%@' \
	| gtail -n +2 | tr -d "\n," \
	| cat <(echo -n "scale=2;") - <(echo "0") | bc
