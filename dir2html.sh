#! /bin/sh
# Generate hypertext menu from directory list
echo "<HEAD>"
echo "<TITLE>Information under: $*</TITLE>"
echo "</HEAD></BODY>"
echo "<H1>$*</H1>"
# If there is a README file include that as plain text
if [ -f $1/README ]; then
    sed -f txt2html.sed  $1/README
fi
# Now generate a list of links to files
echo "<DIR>"
for dir in $*
do (
    cd $dir
    #for file in *.html *.txt
    for file in *
    do 
	echo "<LI><A HREF=./$dir/$file>Title of $file</A>" 
    done
    )
done
echo "</DIR></BODY>"

