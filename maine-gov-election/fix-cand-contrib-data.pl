#!/usr/bin/env perl
use strict;
use warnings;

my $out = '';

$out .= "[";
while (<>) {
    chomp();
    $out .= "\n[";
    s/"\s+"/""/g; s/ +"/"/g; s/\s{2,}/ /g; s/–/-/g; s/Retired[^"]+/Retired/g;
    $out .= $_;
    $out .= "],";
}
chop $out;
$out .= "\n]\n";
print $out;

