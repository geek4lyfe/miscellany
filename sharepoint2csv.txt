# Convert table data from Sharepoint into a CSV. (They make it difficult.)

Load the page in the browser with all the data showing (not paged).
Save the html.
Open the file with an editor and save only the json array containing the rows.
Run this on the file:

cat file | jq -r \
  '(.[0] | keys_unsorted) as $keys | $keys, map([.[ $keys[] ]])[] | @csv' | \
  perl -pe 's#</?(?:div|span|a|font)[^>]*?>##g;' | \
  LC_ALL=C recode -f html | \
  perl -pe 's#<p[^>]*?>##g; s#<(?:br|/p)>(?!["<])#\n#g; s#<(?:br|/p)>##g;'

