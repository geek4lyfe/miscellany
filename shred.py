#!/usr/bin/env python
'''Implement a simple shred-like program.
'''
import sys, os, stat
import numpy # we MUST get REALLY fast random bytes

def shred(fh, fsz):
	'''shred(filehandle, filesize)
	Write random bytes to the given filehandle.
	'''
	nrb = numpy.random.bytes
	bsz = 4096
	total = 0
	for i in xrange(fsz / bsz):
		fh.write(nrb(bsz))
		total += bsz
	fh.write(nrb(fsz - total))

def shred_check(flist):
	'''Check each command line file to ensure it's a regular file and open
	it for writing, call shred to send in the random bytes, then delete.
	'''
	for i in flist:
		try:
			finfo = os.stat(i) # this is good for getting an exc if symlink
			fsz = finfo[6]
			if not stat.S_ISREG(finfo[0]): # check for regular file
				print 'not regular file: %s' % i
				continue
			fh = open(i, 'wb')
			shred(fh, fsz)
			fh.close()
			os.unlink(i)
		except:
			print 'problem writing to file %s' % i

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print 'Write random bytes to one or more files.'
		print 'Usage: %s [file...]' % sys.argv[0]
		sys.exit(1)
	shred_check(sys.argv[1:])

