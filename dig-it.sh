#!/bin/sh
if [ -z "$1" ]; then
  echo "no domain name argument given"
  exit 1
fi
dig +nocmd "$1" any +multiline +noall +answer
dig -tAXFR "$1"

