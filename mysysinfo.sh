#!/bin/sh
# potential tools to use:
tools="uname -a
free
df -h
dmidecode
procinfo -a
cat /proc/cpuinfo
lspci -v
lsmod"
#dmesg"

echo -n "$tools" | xargs -0 | while read tool; do
	which $tool > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo "----- output from $tool -----"
		$tool
		echo ""
	fi
done
