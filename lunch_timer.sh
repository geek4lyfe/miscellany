#!/bin/sh
# like an eggtimer, x popup after delay
# default is to sleep for 1800 sec (30 mins)
timeout=1800
if [ ! -z $1 ]; then
	timeout=$1
fi
echo "sleeping for $timeout seconds"

sleep $timeout && (
    printf "\\a"
	xmessage "time is up - $timeout seconds"
) || (
    echo "maybe $timeout was a bad value"
	exit 1
) &

