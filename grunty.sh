#!/bin/sh
# originally: fswatch -rd assets/js/source assets/css/sass | grunty.sh
# cd /vvv/www/mtm-ms/public_html/wp-content/themes/mainetoday-sunjournal \
cd $HOME/vvv/www/mtm-ms/public_html/wp-content/themes/mainetoday-sunjournal \
    || { echo cannot cd; exit 1; }
fswatch -rd assets/js/source assets/css/sass | \
while read ln; do
    echo $ln;
    grunt
done

