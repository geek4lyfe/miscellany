#!/bin/sh
paths=`echo $PATH | sed -e 's/:/ /g'`
paths=`echo "$paths" | sort | uniq`
if [ -z $1 ]; then
  echo "no command argument supplied (e.g. $0 grep -i prog-pattern)"
  exit 1
fi
cmd=$1
echo $paths
for i in $paths ; do
  echo $i
  #ls $i | $cmd
done

