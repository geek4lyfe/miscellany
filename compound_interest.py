#!/usr/bin/env python
''' calculate compound interest
P = principal, the initial amount of the loan
I = the annual interest rate (from 1 to 100 percent)
L = length, the length (in years) of the loan, or at least the length over which the loan is amortized.
The following assumes a typical conventional loan where the interest is compounded monthly. First I will define two more variables to make the calculations easier:
J = monthly interest in decimal form = I / (12 x 100)
N = number of months over which loan is amortized = L x 12
'''

def compound_interest(P, I, L):
	''' calc monthly payment '''
	P = float(P)
	I = float(I)
	L = float(L)
	J = I / (12 * 100)
	N = L * 12
	M = P * (J / (1 - (1 + J) ** -N))
	return M

if __name__ == '__main__':
	import sys
	print compound_interest(sys.argv[1], sys.argv[2], sys.argv[3])

