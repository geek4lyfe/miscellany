#!/bin/sh
set -x
if [ "x$1" = 'x' ]; then
	echo arg for pgrep needed
	exit 1
fi
cpu=90
if [ "x$2" != 'x' ]; then
	cpu="$2"
fi
pid=-1
while true; do
	pid=`pgrep "$1" | head -1`
	if [ "x$pid" = 'x' ]; then
		sleep .2
		continue
	fi
	cputhrottle "$pid" "$cpu"
	sleep .2
done

