#!/usr/bin/env rscript

data <- read.csv(file='stdin', header=TRUE)
gov_ids <- c(10166,10067,10184,10132,10168,10057,10173,10112,10125,10224,10139,10106)
govdonors <- data[data$OrgID %in% gov_ids,]
govdonors$FullName <- paste(govdonors$FirstName,govdonors$MI, govdonors$LastName,govdonors$Suffix, sep=" ")
govdonors$employment <- paste(govdonors$Occupation,govdonors$Employer, sep="–")
theTable <- govdonors[, c(3,19,2,26,10,11,27)]
#write.csv(theTable, file="stdout", row.names=FALSE, quote=c(1,2,4,5,6,7))
write.csv(theTable, row.names=FALSE, quote=c(1,2,4,5,6,7))

