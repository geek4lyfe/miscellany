#!/bin/sh

grep -Ev '^\s*(#|$)' /etc/apache2/sites-enabled/*.conf | \
  awk '/(Server(Name|Alias)|<Directory )/{ $2=""; print }' | \
  cut -c28- | \
  column -t
