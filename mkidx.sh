#!/bin/bash
unalias -a
mydir=`basename $PWD`
echo -e "<!doctype html>\n<head><title>$mydir</title><style>"
echo 'body { font-family: "Lucida Console", monospace; font-size: 0.8em; } td { text-align: right; padding: 0 10px 0 0; }'
echo '</style></head><body>'
echo "Contents of $mydir<br /><table>"
ls -l | tail -n+2 | while read a b c d size date time name; do
  echo -n "<tr><td>$date</td><td>$time</td><td>"
  printf "%'d" $size
  echo "</td><td style=\"text-align: left\"><a href=\"$name\">$name</a></td></tr>"
done
echo '</table></body>'
