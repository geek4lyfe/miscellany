#!/bin/sh
# '-e .' exports all events.
# '--iphoto "~/Pictures/iPhoto Library"' is assumed.
# -k is to add tags and other metadata. Use -K to scan all files for changes.
# -l is link mode, for local filesystems.
# -f adds face metadata.
# -d -u updates and deletes (like 'rsync -a --delete').
# --pictures ignores movie files

cd /Applications/Phoshare*.app/Contents/MacOS || exit 1
./Phoshare -e . -k -f -d -u --size 1920 \
	--pictures \
	--foldertemplate "{yyyy}/{mm}/{dd}_{plain_name}" \
	--export ~/Dropbox/iPhotoExport

