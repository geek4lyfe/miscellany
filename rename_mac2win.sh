#!/bin/sh
# Rename files from Mac to be more palatable to Windows.
# Double quotes become underscores
# Colons become underscores

# No such file or directory:
# ./B/Bostwick and Company/2010 ads/May 28, 2010-Coastal Scene Ad-color 3 1/4" x 1 1/2".pdf
# ./H/Harbor Management Folder/2010 ads/March 26, 2010-1/8 Page( 3 1/2" x 2 1/2" )Color.pdf
# etc.

function basename() {
   local name="${1##*/}"   
   echo "${name%$2}" 
} 
function dirname() {
   local dir="${1%${1##*/}}"
   "${dir:=./}" != "/" && dir="${dir%?}"
   echo "$dir"
}

# ls | perl -ne "print if /[\x00-\x09\x0E-\x1F\x22-\x25\x7F-\xFF*\/:;?^~|]/"

oldIFS="$IFS"
IFS="\n"

# no trailing spaces
find -d . | grep ' $' | while read path; do
  new="${path%\ }"
  #echo "${path}xyz" "${new}_"
  /bin/mv -n "${path}" "${new}_"
  if [ $? -ne 0 ]; then
    echo "Nope $path"
  fi
done

# no trailing periods
find -d . -name "*." | while read path; do
  new="${path/%./_}"
  # echo "$path" "$new"
  /bin/mv -n "$path" "$new"
  if [ $? -ne 0 ]; then
    echo "Nope $path"
  fi
done

find -d . -name "*:*" | while read path; do
  tmp="${path##*/}"
  bn="${tmp%$path}"
  dn="${path%${path##*/}}"
  # "${dn:=./}" != "/" && dn="${dn%?}"
  new="${bn//:/_}";
  #echo mv "$path" "$dn$new"
  /bin/mv -n "$path" "$dn$new"
  if [ $? -ne 0 ]; then
    echo "Nope $path"
  fi
done
