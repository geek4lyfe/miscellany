#!/bin/sh
set -x
# migrate an Evernote html export to KeepNote

base_dst=~/mytmp/foop

# stop editing beyond this point
read -r -d '' xml_tmpl <<-'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<node>
<version>6</version>
<dict>
  <key>title</key><string>%s</string>
  <key>nodeid</key><string>%s</string>
  <key>modified_time</key><integer>%s</integer>
  <key>version</key><integer>6</integer>
  <key>content_type</key><string>text/xhtml+xml</string>
  <key>created_time</key><integer>%s</integer>
  <key>info_sort_dir</key><integer>1</integer>
  <key>order</key><integer>0</integer>
  <key>info_sort</key><string>order</string>
</dict>
</node>
EOF

for i in *.html; do
  dirnam="${i%.html}";
  mkdir -p "$base_dst/$dirnam";
  cp -p "$i" "$base_dst/$dirnam/";
  if [ -d "$dirnam.resources" ]; then
    cp -pr "$dirnam.resources" "$base_dst/$dirnam/";
  fi;
  #created=`xpath "$i" 'string(//html/head/meta[@name="created"]/@content)' 2>/dev/null`
  created=`xmllint --xpath 'string(//html/head/meta[@name="created"]/@content)' "$i" 2>/dev/null`
  # convert to unix epoch from e.g., 2013-09-10 18:01:37 +0000
  created=`date -jf "%Y-%m-%d %H:%M:%S %z" "$created" +%s`
  #updated=`xpath "$i" 'string(//html/head/meta[@name="updated"]/@content)' 2>/dev/null`
  updated=`xmllint --xpath 'string(//html/head/meta[@name="updated"]/@content)' "$i" 2>/dev/null`
  # convert to unix epoch from e.g., 2013-09-10 18:01:37 +0000
  updated=`date -jf "%Y-%m-%d %H:%M:%S %z" "$updated" +%s`
  node_id=`uuidgen | tr "A-Z" "a-z"`
  # use recode to avoid stuffing xml with invalid data
  title=`echo "$dirnam" | recode utf8..xml`
  # sprintf the vars into the tmpl here
  printf -v node_txt "$xml_tmpl" "$title" "$node_id" "$updated" "$created" 
  echo "$node_txt" > $base_dst/$dirnam/node.xml 
done

# perl -e 'print ((stat(shift))[9] ."\n");'
# xpath something.html 'string(//html/head/meta[@name="created"]/@content)' 2>/dev/null
# 2013-09-10 18:01:37 +0000
# date -jf "%Y-%m-%d %H:%M:%S %z" "$foo" +%s

